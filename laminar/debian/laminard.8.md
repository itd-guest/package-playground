% laminard(1) laminar daemon
% Oliver Giles
% March 19, 2018

# NAME

laminard - laminar daemon

# SYNOPSIS

laminard

# OPTIONS

`laminard` reads the following variables from the environment, which are expected to be sourced by systemd from `/etc/laminar.conf`:

* `LAMINAR_HOME`: The directory in which `laminard` should find job
  configuration and create run directories. Default `/var/lib/laminar`
* `LAMINAR_BIND_HTTP`: The interface/port or unix socket on which `laminard`
  should listen for incoming connections to the web frontend. Default `:8080`
* `LAMINAR_BIND_RPC`: The interface/port or unix socket on which `laminard`
  should listen for incoming commands such as build triggers. Default
  `unix:/var/run/laminar/laminar.sock`
* `LAMINAR_TITLE:` The page title to show in the web frontend.
* `LAMINAR_KEEP_RUNDIRS`: Set to an integer defining how many rundirs to keep
  per job. The lowest-numbered ones will be deleted. The default is 0, meaning
  all run dirs will be immediately deleted.
* `LAMINAR_ARCHIVE_URL`: If set, the web frontend served by `laminard` will use
  this URL to form links to artefacts archived jobs. Must be synchronized with
  web server configuration.

# SCRIPT EXECUTION ORDER

When `$JOB` is triggered on `$NODE`, the following scripts (relative to
`$LAMINAR_HOME/cfg`) may be triggered:

* `jobs/$JOB.init` if the workspace did not exist
* `before`
* `nodes/$NODE.before`
* `jobs/$JOB.before`
* `jobs/$JOB.run`
* `jobs/$JOB.after`
* `nodes/$NODE.after`
* `after`

# ENVIRONMENT VARIABLES

The following variables are available in run scripts:

* `RUN` integer number of this run
* `JOB` string name of this job
* `RESULT` string run status: "success", "failed", etc.
* `LAST_RESULT` string previous run status
* `WORKSPACE` path to this job's workspace
* `ARCHIVE` path to this run's archive

In addition, `$LAMINAR_HOME/cfg/scripts` is prepended to `$PATH`. See helper scripts.

Laminar will also export variables in the form `KEY=VALUE` found in these files:

* `env`
* `nodes/$NODE.env`
* `jobs/$JOB.env`

Finally, variables supplied on the command-line call to `laminarc` start or
`laminarc` trigger will be available. See parameterized builds

# SEE ALSO

`laminarc` (1).
The *README* file distributed with laminar contains full documentation.

The laminar source code and all documentation may be downloaded from
<https://laminar.ohwg.net/>.

