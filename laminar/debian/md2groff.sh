#!/bin/sh
# build groff manpages using pandoc from their markdown source
set -ex
for p in laminarc.1 laminard.8; do
	pandoc -st man "${p}.md" -o "${p}"
done
